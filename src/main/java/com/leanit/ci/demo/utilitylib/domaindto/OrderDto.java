package com.leanit.ci.demo.utilitylib.domaindto;

public class OrderDto {
	public String orderId;
	public String description;

	public OrderDto(String orderId, String description) {
		this.orderId = orderId;
		this.description = description;
	}
}