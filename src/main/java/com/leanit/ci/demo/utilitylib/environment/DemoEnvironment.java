package com.leanit.ci.demo.utilitylib.environment;

public enum DemoEnvironment {
	LOCAL, TOMCAT, KUBERNETES;
}