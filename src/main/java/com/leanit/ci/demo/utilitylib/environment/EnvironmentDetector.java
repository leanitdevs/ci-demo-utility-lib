package com.leanit.ci.demo.utilitylib.environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leanit.common.utilitylib.rabbit.exceptions.UnexpectedInternalErrorException;

public class EnvironmentDetector implements IEnvironmentDetector {
	
	private static Logger logger = LoggerFactory.getLogger(EnvironmentDetector.class);
	
	public static final String ENVIRONMENT_VARIABLE = "cidemo.environment";
	
	public EnvironmentDetector(){
		
	}
	
	@Override
	public DemoEnvironment detect() {
		logger.info("Check if system variable {} exists...", ENVIRONMENT_VARIABLE);
		String envName = System.getProperty(ENVIRONMENT_VARIABLE);
		if(envName != null){
			logger.info("Key found. Retrieving environment {}", envName);
			return DemoEnvironment.valueOf(envName.toUpperCase());
		} else {
			throw new UnexpectedInternalErrorException("No env variable found. Is not possible to obtain the env");
		}
	}
}