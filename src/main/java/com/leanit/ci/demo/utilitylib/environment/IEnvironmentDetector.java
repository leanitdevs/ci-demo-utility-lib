package com.leanit.ci.demo.utilitylib.environment;

public interface IEnvironmentDetector {
	DemoEnvironment detect();
}