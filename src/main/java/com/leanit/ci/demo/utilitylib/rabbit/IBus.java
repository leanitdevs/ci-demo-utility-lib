package com.leanit.ci.demo.utilitylib.rabbit;

public interface IBus {
	<T> void publish(T msg);
	<T> void subscribeConsumer(IConsumer<T> consumer);
}