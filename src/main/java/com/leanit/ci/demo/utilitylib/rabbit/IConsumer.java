package com.leanit.ci.demo.utilitylib.rabbit;

public interface IConsumer<T> {
	void consume(T msg);
	String getConsumerName();
	Class<T> getMsgClass();
}