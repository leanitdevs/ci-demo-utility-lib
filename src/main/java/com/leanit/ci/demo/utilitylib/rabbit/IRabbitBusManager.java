package com.leanit.ci.demo.utilitylib.rabbit;

public interface IRabbitBusManager {
	/**
	 * Connect to RabbitMQ broker and return a useful interface for interacting with it
	 * @return The IBus for interacting with the RabbitMQ broker
	 */
	IBus start();
	/**
	 * Close the connection to the RabbitMQ broker
	 */
	void stop();
}