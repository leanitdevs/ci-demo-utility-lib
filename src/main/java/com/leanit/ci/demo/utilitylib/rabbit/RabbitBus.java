package com.leanit.ci.demo.utilitylib.rabbit;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.leanit.common.utilitylib.rabbit.exceptions.UnexpectedInternalErrorException;
import com.leanit.common.utilitylib.rabbit.utils.ChannelUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class RabbitBus implements IBus {
	 
	private static final Logger log = LoggerFactory.getLogger(RabbitBus.class);

	private Connection connection;
	private Gson gson;
	private static final int QOS = 1;

	private String queueName;

	public RabbitBus(Connection connection, String queueName) {
		this.connection = connection;
		this.queueName = queueName;
		this.gson = new Gson();
	}

	@Override
	public <T> void publish(T msg) {
		try {
			Channel channel = this.createChannel();
			ChannelUtils.initQueue(channel, queueName);
			channel.basicPublish("",
					queueName, 
					null,
					gson.toJson(msg).getBytes());
			channel.close();
		} catch (Exception ex) {
			String errorMsg = "Unexpected internal error in RabbitMQ bus";
			log.error(errorMsg, ex);
			throw new UnexpectedInternalErrorException(errorMsg, ex);
		}
	}


	@Override
	public <T> void subscribeConsumer(final IConsumer<T> consumer) {
		try {
			final Channel channel = this.createChannel();
			ChannelUtils.initQueue(channel, queueName);
			channel.basicConsume(queueName, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					
					String jsonMsg = null;
					
					try {
					
						jsonMsg = new String(body, StandardCharsets.UTF_8);
						
						log.debug("Consumer={} consuming msg={} from queue={}", consumer.getConsumerName(), jsonMsg, queueName);

						T pojoMsg = gson.fromJson(jsonMsg, consumer.getMsgClass());
						consumer.consume(pojoMsg);
						
						channel.basicAck(envelope.getDeliveryTag(), false);
					} catch(Exception e) {
						e.printStackTrace();
						log.error("Consumer={} consuming msg={} from queue={} failed", consumer.getConsumerName(), jsonMsg, queueName);
						
						channel.basicNack(envelope.getDeliveryTag(), 
								false, 									// multiple=false, nack only this msg
								false); 								// requeue=false, dead-lettered or discarded on the basis of storeUnprocessedMesssagesInErrorQueue param 
					}
				}
			});
		} catch(Exception ex) {
			String errorMsg = "Unexpected internal error in RabbitMQ bus";
			log.error(errorMsg, ex);
			throw new UnexpectedInternalErrorException(errorMsg, ex);
		}
	}
	
	private Channel createChannel(){
		try {
			log.debug("Creating new channel");
			Channel channel = this.connection.createChannel();
			channel.basicQos(QOS);
			return channel;
		} catch (Exception e) {
			throw new RuntimeException("Rabbitmq internal problem",e);
		}
	}
}