package com.leanit.ci.demo.utilitylib.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitBusManager implements IRabbitBusManager {
	
	static final Logger log = LoggerFactory.getLogger(RabbitBusManager.class);
 
	private ConnectionFactory connectionFactory;
	private RabbitConfiguration config;
	private Connection connection;

	/**
	 * Will create a connection to the Rabbit message broker with the provided config
	 * @param config
	 */
	public RabbitBusManager(RabbitConfiguration config) {
		this.config = config;
	}

	@Override
	public IBus start() {
		this.connectionFactory = new ConnectionFactory();
		this.connectionFactory.setHost(config.host);
		this.connectionFactory.setPort(config.port);
		this.connectionFactory.setPassword(config.password);
		this.connectionFactory.setUsername(config.username);
		this.connectionFactory.setAutomaticRecoveryEnabled(config.automaticRecovery);
		this.connectionFactory.setConnectionTimeout(config.connectionTimeout);

		try {
			this.connection = connectionFactory.newConnection();
			return new RabbitBus(connection, config.queueName);
		} catch (Exception e) {
			log.error("Could not create new Rabbitmq connection with {}:{} [{}/{}]", config.host, config.port, config.username, config.password);
			log.error("Exception cause {} ", e.getCause());
			log.error("Exception message {} ", e.getMessage());
			log.error("Exception ", e);
			throw new RuntimeException("Could not create new Rabbitmq connection with "+config.host+":"+config.port, e); 
		}
	}
	
	@Override
	public void stop() {
		try {
			// Close connection here!
			this.connection.close();
		} catch (Exception e) {
			log.error("Could not close Rabbitmq connection", e);
			throw new RuntimeException("Could not close Rabbitmq connection", e); 
		}
	}
}