package com.leanit.common.utilitylib.rabbit.exceptions;

public class UnexpectedInternalErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnexpectedInternalErrorException(String errorMsg) {
		super(errorMsg);
	}

	public UnexpectedInternalErrorException(String errorMsg, Exception e) {
		super(errorMsg, e);
	}
}