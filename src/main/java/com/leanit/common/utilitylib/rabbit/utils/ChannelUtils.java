package com.leanit.common.utilitylib.rabbit.utils;

import java.io.IOException;

import com.rabbitmq.client.Channel;

public class ChannelUtils {

	public static void initQueue(Channel channel, String consumerQueueName) throws IOException {
		channel.queueDeclare(consumerQueueName, 
				true, 												// durable, will survive server restart
				false, 												// not auto-delete, the system will not delete the queue even if nobody is using it
				false,												// not exlusive		
				null);												// No additional arguments
	}
}