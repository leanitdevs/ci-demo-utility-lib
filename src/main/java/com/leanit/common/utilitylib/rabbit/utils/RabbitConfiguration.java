package com.leanit.common.utilitylib.rabbit.utils;

import com.rabbitmq.client.ConnectionFactory;

public class RabbitConfiguration {
	
	public String host;
	public String username;
	public Integer port;
	public String password;
	public boolean automaticRecovery;
	public int connectionTimeout;
	public String queueName;
	
	public RabbitConfiguration(String host, Integer port, String username, String password, String queue)	{
		this(host, port, username, password, ConnectionFactory.DEFAULT_CONNECTION_TIMEOUT, true, queue);
	}
	
	public RabbitConfiguration(String host, Integer port, String username, String password, int connectionTimeout, boolean autoRecover, String queue) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.connectionTimeout = connectionTimeout;
		this.automaticRecovery = autoRecover;
		this.queueName = queue;
	}

	@Override
	public String toString() {
		return "RabbitConfiguration [host=" + host + ", username=" + username + ", port=" + port + ", password="
				+ password + ", automaticRecovery=" + automaticRecovery + ", connectionTimeout=" + connectionTimeout
				+ ", queueName=" + queueName + "]";
	}
}