package com.leanit.ci.demo.utilitylib.environment;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.leanit.common.utilitylib.rabbit.exceptions.UnexpectedInternalErrorException;

public class EnvironmentDetectorTest {

	private static String originalSystemValue;
	
	@BeforeClass
	public static void beforeClass(){
		originalSystemValue = System.clearProperty(EnvironmentDetector.ENVIRONMENT_VARIABLE);
	}
	
	@AfterClass
	public static void afterClass(){
		if(originalSystemValue != null){
			System.setProperty(EnvironmentDetector.ENVIRONMENT_VARIABLE, originalSystemValue);
		}
	}

	@Test(expected=UnexpectedInternalErrorException.class)
	public void verifyExceptionIsThrownWhenNoVariableIsSet() {
		EnvironmentDetector detector = new EnvironmentDetector();
		detector.detect();
	}

	@Test
	public void test__LOCAL__EnvironmentIsFound() {
		setUpProperty(DemoEnvironment.LOCAL.toString());
		EnvironmentDetector detector = new EnvironmentDetector();
		DemoEnvironment environment = detector.detect();
		
		assertEquals(environment, DemoEnvironment.LOCAL);
		
		rollBackProperty();
	}

	@Test
	public void test__DEV__EnvironmentIsFound() {
		setUpProperty(DemoEnvironment.TOMCAT.toString());
		EnvironmentDetector detector = new EnvironmentDetector();
		DemoEnvironment environment = detector.detect();
		
		assertEquals(environment, DemoEnvironment.TOMCAT);
		 
		rollBackProperty();
	}

	@Test
	public void test__PROD__EnvironmentIsFound() {
		setUpProperty(DemoEnvironment.KUBERNETES.toString());
		EnvironmentDetector detector = new EnvironmentDetector();
		DemoEnvironment environment = detector.detect();
		
		assertEquals(environment, DemoEnvironment.KUBERNETES);
		
		rollBackProperty();
	}

	private static String setUpProperty(String value){
		return System.setProperty(EnvironmentDetector.ENVIRONMENT_VARIABLE, value);
	}
	
	private static void rollBackProperty(){
		System.clearProperty(EnvironmentDetector.ENVIRONMENT_VARIABLE);
	}
}