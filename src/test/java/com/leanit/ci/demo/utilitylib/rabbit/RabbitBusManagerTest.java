package com.leanit.ci.demo.utilitylib.rabbit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.leanit.ci.demo.utilitylib.rabbit.IRabbitBusManager;
import com.leanit.ci.demo.utilitylib.rabbit.RabbitBusManager;
import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;

@RunWith(MockitoJUnitRunner.class)
public class RabbitBusManagerTest {

	@Test(expected=RuntimeException.class)
	public void rabbitBusManager_should_not_be_closed_more_than_once() {
		RabbitConfiguration config = new RabbitConfiguration("", -1, "", "", "");
		
		IRabbitBusManager busManager = new RabbitBusManager(config);
		busManager.start();
		busManager.stop();
		busManager.stop();
	}
}