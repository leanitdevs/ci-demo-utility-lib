package com.leanit.ci.demo.utilitylib.rabbit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.leanit.ci.demo.utilitylib.rabbit.IBus;
import com.leanit.ci.demo.utilitylib.rabbit.IConsumer;
import com.leanit.ci.demo.utilitylib.rabbit.RabbitBus;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;


@RunWith(MockitoJUnitRunner.Silent.class)
public class RabbitBusTest {

	@Mock(answer = Answers.RETURNS_DEEP_STUBS) private Connection connection;
	@Mock private IConsumer<String> consumer;
	private String queueName = "queue";
	
	@Before
	public void initMocks() {
		when(consumer.getConsumerName()).thenReturn("dummyConsumer");
		when(consumer.getMsgClass()).thenReturn(String.class);
	}
	
	@Test
	public void testInvocationsFor__subscribeConsumer() throws IOException {
		IBus bus = new RabbitBus(connection, queueName);
		
		bus.subscribeConsumer(consumer);
		
		verify(connection.createChannel(), times(1)).basicConsume(anyString(), any(Consumer.class));
	}
	
	@Test
	public void testInvocationsFor__publish() throws IOException {
		IBus bus = new RabbitBus(connection, queueName);
		String dummyMSg = "my message";
		
		bus.publish(dummyMSg);
		
		verify(connection.createChannel(), times(1)).basicPublish(eq(""), eq(queueName), eq(null), eq(new Gson().toJson(dummyMSg).getBytes()));
	}
}