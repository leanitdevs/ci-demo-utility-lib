package com.leanit.ci.demo.utilitylib.rabbit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;

@Ignore
public class TestSimpleSendReceive {

	private static final String QUEUE_NAME = "simple-rabbit-lib-queue";
	private static final int POJO_VALUE = 1;
	private static final String POJO_KEY = "pojokey";

	@Test
	public void testSendReceive() throws InterruptedException {
		IRabbitBusManager busManager = new RabbitBusManager(new RabbitConfiguration("localhost", 5672, "cidemo", "cidemo123", QUEUE_NAME));
		IBus bus = busManager.start();
		
		final Store<Pojo> pojoWrapper = new Store<>();
		bus.subscribeConsumer(new IConsumer<Pojo>() {
			@Override
			public void consume(final Pojo pojo) {
				pojoWrapper.obj = pojo;
			}
			@Override
			public String getConsumerName() {
				return "dummy";
			}
			@Override
			public Class<Pojo> getMsgClass() {
				return Pojo.class;
			}
		});
		
		bus.publish(new Pojo(POJO_KEY, POJO_VALUE));
		
		Thread.sleep(1000);
		busManager.stop();
		
		assertThat(pojoWrapper.obj.key, is(POJO_KEY));
		assertThat(pojoWrapper.obj.value, is(POJO_VALUE));
	}
	
	class Pojo {
		public String key;
		public Integer value;
		
		public Pojo(String key, Integer value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public String toString() {
			return "Pojo [key=" + key + ", value=" + value + "]";
		}
	}
}