package com.leanit.ci.demo.utilitylib.rabbit.utils;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.leanit.common.utilitylib.rabbit.utils.ChannelUtils;
import com.rabbitmq.client.Channel;


@RunWith(MockitoJUnitRunner.class)
public class ChannelUtilsTest {

	@Mock private Channel channel;
	private String consumerQueueName = "dummyQueueName";

	
	@Test
	public void testInvocations__initConsumerQueue() throws IOException {
		ChannelUtils.initQueue(channel, consumerQueueName);
		
		verify(channel, times(1)).queueDeclare(eq(consumerQueueName), eq(true), eq(false), eq(false), eq(null));
	}
}
